#Author-Dag Rende
#Description-Generate Simplified STL export of bodies

import os, sys
from collections import defaultdict

import adsk.core, adsk.fusion
import traceback
import re

from . import utils
from . import addin

def executor(addIn):
    if not addIn.design:
        raise RuntimeError('No active Fusion design')
    try:
        # export stl
        exportMgr = addIn.design.exportManager
        scriptDir = os.path.dirname(os.path.realpath(__file__))  

        # get document name - including version if checkbox checked
        documentName = addIn.design.parentDocument.name
        if not addIn.attributes['includeVersion']:
            match = re.search("(.*) v\d+", documentName)
            if match:
                documentName = match.group(1)

        # get directory to export to from field
        destDir = addIn.attributes['destinationDirectory'].rstrip('/')

        # export each body
        for body in addIn.attributes['exportEntities']:
            fileName = destDir + '/' + documentName + ' - ' + body.name
            stlExportOptions = exportMgr.createSTLExportOptions(body, fileName)
            stlExportOptions.sendToPrintUtility = False
            exportMgr.execute(stlExportOptions)
    except:
        utils.messageBox(traceback.format_exc())

params = [
    addin.SelectionParam('exportEntities', 'Bodies', 'Bodies to export', 'click all bodies to export'),
    addin.StringParam('destinationDirectory', '', 'Destination directory', 'Directory to write the STL files to'),
    addin.BoolParam('includeVersion', False, 'Include version', 'Include version in the first part of the STL filename')
]

addIn = addin.AddIn("exportStls", 'ExportStls', params)
addIn.description = 'Export all selected bodies as STL files.'
addIn.executor = executor

def run(context):
    try:
        addIn.addButton()
    except:
        utils.messageBox(traceback.format_exc())


def stop(context):
    try:
        addIn.removeButton()
    except:
        utils.messageBox(traceback.format_exc())

# Export STLs

I create hundreds of Fusion 360 models for 3D printing.
But why is it so laborous to export the created models to STL files to slice and print? And why do you have to rename each body to get the files unique in the output directory?

Thid add-on let you click the STL button, click on each body to export and click OK. Each body is now exported to an STL file with name built from the model document name and the body name.

## Install it

1. Open https://gitlab.com/findout/export-stl in a web browser
1. Click
![download button](Resources/download-button.png)
and select ZIP and the file export-stl-master.zip gets downloaded.
1. Unzip the file somewhere on your disk.
1. Open Fusion 360
1. Click the UTILITIES tab at top of window
1. Click ADD-INS
1. Click the Add-Ins tab
1. Click the + button to the right of My Add-Ins
1. Choose the folder you unzipped to above
1. The item ExportStls should appear in the list
1. Click ExportStls and click the Run button below the list
1. Now click the SOLID tab at window top
1. You should see the button
![STLS](Resources/STL-button.png)
1. Now test this add-on: open a model and click the STL button, to open the export window:
 ![export stl dialog](Resources/export-stl-dialog.png)
1. Fill in the path to the folder where you want your STL files (I use /Users/dag/Dropbox/stls)
1. Select one or more Body objects and click OK
1. Now the STL file(s) should be in the destination folder

